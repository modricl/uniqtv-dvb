package com.uniqcast.uniqtvdvb;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ChannelScan extends Activity {

    private static final String TAG = "[uniqtv-dvb-java][scan]";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel_scan);

        // get data from intent or set default values if testing activity directly
        Intent intent = getIntent();
        String frequency = intent.getStringExtra("FREQUENCY");
        String symbol = intent.getStringExtra("SYMBOL");
        String modulation = intent.getStringExtra("MODULATION");
        final int mod = (modulation == null || modulation.isEmpty()) ? 0 : Integer.parseInt(modulation) - 1;

        final TextView frequencyView = (TextView) findViewById(R.id.frequency);
        frequencyView.setText((frequency == null || frequency.isEmpty()) ? "32301" : frequency);

        final TextView symbolView = (TextView)  findViewById(R.id.symbol);
        symbolView.setText((symbol == null || symbol.isEmpty()) ? "6785" : symbol);

        final Spinner modView = (Spinner) findViewById(R.id.modulation);
        modView.setSelection(mod);

        Button scanChannelsButton = (Button) findViewById(R.id.scan_channels);
        scanChannelsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int frequency = Integer.parseInt(frequencyView.getText().toString());
                int symbol = Integer.parseInt(symbolView.getText().toString());
                int modulation = modView.getSelectedItemPosition() + 1;

                Log.d(TAG, "start channel scan: " + frequency + " " + symbol + " " + modulation);

                UnionManPlayer.startSearch(frequency, symbol, modulation);
                Toast.makeText(getApplicationContext(), "Scan started!", Toast.LENGTH_SHORT).show();
            }
        });

        Button clearChannelsButton = (Button) findViewById(R.id.clear_channels);
        clearChannelsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "clear channel list");

                UnionManPlayer.deleteChannels();
                Toast.makeText(getApplicationContext(), "All channels removed!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
