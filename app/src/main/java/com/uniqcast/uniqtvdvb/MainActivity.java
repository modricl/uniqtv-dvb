package com.uniqcast.uniqtvdvb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {
    private static final String TAG = "[uniqtv-dvb-java][main]";

    private ListView mChannelView = null;
    private ListView mMenuView = null;
    private UnionManPlayer player = null;
    private TextView mSignalLevel = null;
    private TextView mSignalStrength = null;
    private TextView mCurrentFrequency = null;

    private List<String> mChannelList;
    private List<String> mMenuList;
    private ArrayAdapter<String> mChannelListAdapter = null;
    private ArrayAdapter<String> mMenuListAdapter = null;
    private int currentChannel = 0;

    private Handler handler = new Handler();
    private Runnable signalRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        initViews();

        if (mChannelList.size() > 0)
            playChannel(currentChannel);
        else
            openScanActivity();

        signalRunnable = new Runnable() {
            @Override
            public void run() {
                mSignalLevel.setText(String.valueOf(player.getSignalLevel()));
                mSignalStrength.setText(String.valueOf(player.getSignalStrength()));
                mCurrentFrequency.setText("(" + String.valueOf(player.getCurrentFrequency()) + ")");
                handler.postDelayed(signalRunnable, 3000);
            }
        };
        handler.post(signalRunnable);
    }

    private void initData() {
        player = new UnionManPlayer(0, this);
        mChannelList = player.getChannels();
        mMenuList = new ArrayList<String>(Arrays.asList("Channel Scan", "EPG", "Quit"));
    }

    private void initViews() {
        mSignalLevel = (TextView) findViewById(R.id.signal_level);
        mSignalStrength = (TextView) findViewById(R.id.signal_strength);
        mCurrentFrequency = (TextView)  findViewById(R.id.current_frequency);
        mChannelView = (ListView) findViewById(R.id.channel_list);
        mMenuView = (ListView) findViewById(R.id.menu_list);

        mChannelListAdapter = new ArrayAdapter<String>(this,
                android.R.layout.activity_list_item, android.R.id.text1, mChannelList);
        mMenuListAdapter = new ArrayAdapter<String>(this,
                android.R.layout.activity_list_item, android.R.id.text1, mMenuList);

        mChannelView.setAdapter(mChannelListAdapter);
        mMenuView.setAdapter(mMenuListAdapter);

        mChannelView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                playChannel(position);
            }
        });

        mMenuView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Log.d(TAG, "menu item clicked:" + position);

                switch(position)
                {
                    // SCAN
                    case 0:
                        openScanActivity();
                    break;

                    // EPG
                    case 1:
                        Log.d(TAG, "open epg activity: not implemented");
                    break;

                    // Quit
                    case 2:
                        System.exit(0);
                    break;
                }
            }
        });

        mChannelView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyEvent.getAction() == KeyEvent.ACTION_UP)
                    return false;

                Log.d(TAG, "channel key pressed:" + keyCode);

                switch (keyCode) {
                    case KeyEvent.KEYCODE_BACK:
                        closeChannels();
                        return true;

                    case KeyEvent.KEYCODE_PAGE_DOWN:
                        channelUp();
                        openChannels();
                        return true;

                    case KeyEvent.KEYCODE_PAGE_UP:
                        channelDown();
                        openChannels();
                        return true;

                }
                return false;
            }
        });

        mMenuView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {

                if (keyEvent.getAction() == KeyEvent.ACTION_UP)
                    return false;

                Log.d(TAG, "menu key pressed:" + keyCode);

                switch (keyCode) {
                    case KeyEvent.KEYCODE_BACK:
                    case KeyEvent.KEYCODE_MENU:
                        closeMenu();
                        return true;
                }
                return false;
            }
        });
    }

    private void playChannel(int id) {
        currentChannel = id;
        player.playChannel(id);
    }

    private void channelUp()
    {
        if (++currentChannel > mChannelList.size())
            currentChannel = 1;

        playChannel(currentChannel);
    }

    private void channelDown()
    {
        if (--currentChannel <= 0)
            currentChannel = mChannelList.size();

        playChannel(currentChannel);
    }

    private void openChannels()
    {
        mChannelView.setSelection(currentChannel - 1);
        if (mChannelView.getVisibility() != View.VISIBLE)
        {
            mChannelView.setVisibility(View.VISIBLE);
            mChannelView.requestFocus();
        }
    }

    private void closeChannels()
    {
        if (mChannelView.getVisibility() == View.VISIBLE)
        {
            mChannelView.setVisibility(View.INVISIBLE);
            mChannelView.clearFocus();
        }
    }

    private void openMenu()
    {
        if (mMenuView.getVisibility() != View.VISIBLE)
        {
            mMenuView.setVisibility(View.VISIBLE);
            mMenuView.requestFocus();
        }
    }

    private void closeMenu()
    {
        if (mMenuView.getVisibility() == View.VISIBLE)
        {
            mMenuView.setVisibility(View.INVISIBLE);
            mMenuView.clearFocus();
        }
    }

    private void openScanActivity()
    {
        Log.d(TAG, "open scan activity");

        // open scan activity and send current  information
        Intent intent = new Intent(this, ChannelScan.class);
        intent.putExtra("FREQUENCY", String.valueOf(player.getDefaultFrequency()));
        intent.putExtra("SYMBOL", String.valueOf(player.getDefaultSymbol()));
        intent.putExtra("MODULATION", String.valueOf(player.getDefaultModulation()));
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected  void onResume()
    {
        Log.d(TAG, "update channel list");

        mChannelListAdapter.clear();
        mChannelListAdapter.addAll(player.getChannels());

        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        Log.d(TAG, "key pressed:" + event.toString());

        switch (keyCode)
        {
            // subtitle key
            case KeyEvent.KEYCODE_F5:
//                getSubtitle();
                break;

            // audio key
            case KeyEvent.KEYCODE_F6:
//                getCaption();
                break;

            // OK key
            case KeyEvent.KEYCODE_DPAD_CENTER:
            case KeyEvent.KEYCODE_ENTER:
                openChannels();
                break;

            case KeyEvent.KEYCODE_PAGE_DOWN:
                channelUp();
                openChannels();
                break;

            case KeyEvent.KEYCODE_PAGE_UP:
                channelDown();
                openChannels();
                break;

            case KeyEvent.KEYCODE_DPAD_DOWN:
            case KeyEvent.KEYCODE_DPAD_UP:
                openChannels();
                break;

            case KeyEvent.KEYCODE_MENU:
                closeChannels();
                openMenu();
                return true;

        }

        return super.onKeyDown(keyCode, event);
    }
}
